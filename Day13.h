#pragma once
#include <string>
#include <vector>
class Day13
{
public:
	Day13();
	~Day13();
	int Part1();
	int Part2();
private:
	struct pos
	{
		long long x = 0;
		long long y = 0;
	};
	struct CPU
	{
		long long*p1 = nullptr;
		long long*p2 = nullptr;
		long long*p3 = nullptr;
		int rBase = 0;
		std::vector<long long> intcode;
		int pos = 0;
		CPU(std::vector<long long> code) : intcode(code)
		{
			intcode.resize(5000, 0L);
		};
		bool run(std::vector<long long> input, long long& output);
		void loadParams(long long operation, int numberToLoad);
		long long* loadParam(int parammode, int pos);
	};
	int compare(long long a, long long b);
	void loadData(std::fstream & stream, std::vector<long long> & data);
	std::string m_filename = "Data/Day13.txt";
};
