#include "Day10.h"
#include <set>
#include <map>

Day10::Day10()
{
	std::fstream datafile(m_filename, std::fstream::in);
	loadData(datafile, m_asteroids);
}

Day10::~Day10()
{
}

int Day10::Part1()
{
	int max = 0;
	for (asteroid& asteroid1 : m_asteroids)
	{
		std::set<double> grads;
		for (asteroid& asteroid2 : m_asteroids)
		{
			if (asteroid1.x != asteroid2.x || asteroid1.y != asteroid2.y)
			{
				grads.insert(getAngle(asteroid1, asteroid2));
			}
		}
		if (grads.size() > max)
		{
			max = grads.size();
			m_scannerLocation = asteroid1;
		}
	}
	return max;
}

int Day10::Part2()
{
	std::map<double, std::vector<asteroid>> asteroidMap;
	for (asteroid& asteroid : m_asteroids)
	{
		if (m_scannerLocation.x != asteroid.x || m_scannerLocation.y != asteroid.y)
		{
			asteroid.angle = getAngle(m_scannerLocation, asteroid);
			asteroid.dist = getDist(m_scannerLocation, asteroid);
			asteroidMap[asteroid.angle].push_back(asteroid);
		}
	}
	int i = 0;
	while (i < 200)
	{
		for (auto asteroidsAtAngle : asteroidMap)
		{
			asteroid* nearest = nullptr;
			for (auto& ast : asteroidsAtAngle.second)
			{
				if (!ast.boom && (nearest == nullptr || ast.dist < nearest->dist))
					nearest = &ast;
			}
			if (nearest != nullptr)
			{
				nearest->boom = true;
				if (++i == 200)
					return (100 * nearest->x) + nearest->y;
			}
		}
	}
	return 0;
}

double Day10::getAngle(asteroid pos1, asteroid pos2)
{
	double ret = atan2(pos2.x - pos1.x, -(pos2.y - pos1.y));
	while (ret < 0)
		ret += _2PI;
	while (ret >= _2PI)
		ret -= _2PI;
	return ret;
}

double Day10::getDist(asteroid pos1, asteroid pos2)
{
	return sqrt(pow(pos2.x - pos1.x, 2) + pow(pos2.y - pos1.y, 2));
}

void Day10::loadData(std::fstream & stream, std::vector<asteroid>& data)
{
	int y = 0;
	for (std::string line; getline(stream, line); )
	{
		int x = 0;
		for (char pos : line)
		{
			if (pos == '#')
				data.push_back({ x,y });
			x++;
		}
		y++;
	}
}
