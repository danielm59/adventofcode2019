#pragma once
#include <vector>
#include <fstream>
#include <string>
#include <queue>

class Day17
{
public:
	Day17();
	~Day17();
	int Part1();
	int Part2();
private:
	struct CPU
	{
		long long*p1 = nullptr;
		long long*p2 = nullptr;
		long long*p3 = nullptr;
		int rBase = 0;
		std::vector<long long> intcode;
		int pos = 0;
		CPU(std::vector<long long> code) : intcode(code)
		{
			intcode.resize(4096, 0L);
		};
		bool run(std::queue<long long>& input, long long& output);
		void loadParams(long long operation, int numberToLoad);
		long long* loadParam(int parammode, int pos);
	};
	std::vector<std::string> m_map = { "" };
	void addInput(const std::string& s, std::queue<long long>& input);
	void loadData(std::fstream & stream, std::vector<long long> & data);
	std::string m_filename = "Data/Day17.txt";
};

