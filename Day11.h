#pragma once
#include <string>
#include <vector>
class Day11
{
public:
	Day11();
	~Day11();
	int Part1();
	void Part2();
private:
	enum Direction {Up, Right, Down, Left };
	struct Robot
	{
		int x = 0;
		int y = 0;
		Direction facing = Up;
		void clockwise() 
		{
			facing = Direction((facing + 1) % 4);
		}
		void anticlockwise()
		{
			facing = Direction((facing + 3) % 4);
		}
		void move()
		{
			switch (facing)
			{
			case Up:
				y--;
				break;
			case Right:
				x++;
				break;
			case Down:
				y++;
				break;
			case Left:
				x--;
				break;
			}
		}
	};
	struct CPU
	{
		long long*p1 = nullptr;
		long long*p2 = nullptr;
		long long*p3 = nullptr;
		int rBase = 0;
		std::vector<long long> intcode;
		int pos = 0;
		CPU(std::vector<long long> code) : intcode(code)
		{
			intcode.resize(5000, 0L);
		};
		bool run(std::vector<long long> input, long long& output);
		void loadParams(long long operation, int numberToLoad);
		long long* loadParam(int parammode, int pos);
	};
	void loadData(std::fstream & stream, std::vector<long long> & data);
	std::string m_filename = "Data/Day11.txt";
};

