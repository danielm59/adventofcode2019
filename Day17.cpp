#include "Day17.h"
#include <iostream>

Day17::Day17()
{
}

Day17::~Day17()
{
}

int Day17::Part1()
{
	std::fstream datafile(m_filename, std::fstream::in);
	std::vector<long long> intcode;
	loadData(datafile, intcode);
	CPU cpu(intcode);
	bool con = true;
	std::queue<long long> input;
	long long output;
	std::string* currentLine = &m_map.back();
	while (con)
	{
		con = cpu.run(input, output);
		if (con)
			if (output == '\n')
			{
				m_map.push_back("");
				currentLine = &m_map.back();
			}
			else
			{
				currentLine->operator+=((char)output);
			}
	}
	int total = 0;
	for (int i = 1; i < m_map.size()-1; i++)
	{
		std::string& line = m_map[i];
		for (int j = 1; j < line.size(); j++)
		{
			if (line[j] == '#')
			{
				if (line[j - 1] == '#' && line[j + 1] == '#' && m_map[i - 1][j] == '#' && m_map[i + 1][j] == '#')
				{
					total += i * j;
				}
			}
		}
	}
	return total;
}

int Day17::Part2()
{
	std::fstream datafile(m_filename, std::fstream::in);
	std::vector<long long> intcode;
	loadData(datafile, intcode);
	intcode[0] = 2;
	CPU cpu(intcode);
	//zipped by hand
	std::string main = "A,B,A,C,B,C,B,A,C,B\n";
	std::string A = "L,6,R,8,R,12,L,6,L,8\n";
	std::string B = "L,10,L,8,R,12\n";
	std::string C = "L,8,L,10,L,6,L,6\n";
	std::queue<long long> input;
	addInput(main, input);
	addInput(A, input);
	addInput(B, input);
	addInput(C, input);
	addInput("n\n", input); //turn off camera
	long long output;
	bool con = true;
	while (con)
	{
		con = cpu.run(input, output);
	}
	return output;
}

void Day17::addInput(const std::string& s, std::queue<long long>& input)
{
	for (int it = 0; it < s.size(); it++)
	{
		input.push((long long)s[it]);
	}
}

void Day17::loadData(std::fstream & stream, std::vector<long long>& data)
{
	long long i = 0L;
	char c;
	while (stream >> i >> c)
	{
		data.push_back(i);
	}
	stream >> i;
	data.push_back(i);
}

bool Day17::CPU::run(std::queue<long long>& input, long long& output)
{
	for (int& i = pos; i < intcode.size();)
	{
		long long operation = intcode[i];
		int opcode = operation % 100;
		switch (opcode)
		{
		case 1:
			loadParams(operation, 3);
			*p3 = *p1 + *p2;
			i += 4;
			break;
		case 2:
			loadParams(operation, 3);
			*p3 = *p1 * *p2;
			i += 4;
			break;
		case 3:
			loadParams(operation, 1);
			*p1 = input.front();
			input.pop();
			i += 2;
			break;
		case 4:
			loadParams(operation, 1);
			output = *p1;
			i += 2;
			return true;
			break;
		case 5:
			loadParams(operation, 2);
			i = *p1 ? *p2 : i + 3;
			break;
		case 6:
			loadParams(operation, 2);
			i = *p1 ? i + 3 : *p2;
			break;
		case 7:
			loadParams(operation, 3);
			*p3 = (*p1 < *p2) ? 1 : 0;
			i += 4;
			break;
		case 8:
			loadParams(operation, 3);
			*p3 = (*p1 == *p2) ? 1 : 0;
			i += 4;
			break;
		case 9:
			loadParams(operation, 1);
			rBase += *p1;
			i += 2;
			break;
		case 99:
			return false;
		default:
			std::cout << "Error, Unknow Opcode:" << i << "," << opcode << std::endl;
			return intcode[0];
			break;
		}
	}
	std::cout << "Error, Reached End:" << pos << std::endl;
	return intcode[0];
}

void Day17::CPU::loadParams(long long operation, int numberToLoad)
{
	switch (numberToLoad)
	{
	case 3:
		p3 = loadParam(operation / 10000, pos + 3);
	case 2:
		p2 = loadParam((operation / 1000) % 10, pos + 2);
	case 1:
		p1 = loadParam((operation / 100) % 10, pos + 1);
	}
}

long long * Day17::CPU::loadParam(int parammode, int pos)
{
	switch (parammode)
	{
	case 0:
		return &intcode[intcode[pos]];
	case 1:
		return &intcode[pos];
	case 2:
		return &intcode[rBase + intcode[pos]];
	}
}