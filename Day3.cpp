#include "Day3.h"
#include <sstream>
#include <iostream>
#include<algorithm> 

Day3::Day3()
{
	std::fstream datafile(m_filename, std::fstream::in);
	std::vector <std::vector<std::pair<char, int>>> moveData;
	loadData(datafile, moveData);

	//wire 1
	std::vector<Line> wire1;
	createWire(wire1, moveData[0]);

	//wire 2
	std::vector<Line> wire2;
	createWire(wire2, moveData[1]);

	Pos intersection;
	int shortest = INT_MAX;
	for (auto it1 : wire1)
	{
		for (auto it2 : wire2)
		{
			if (TestIntersection(it1, it2, intersection))
			{
				//For part 1
				int dist = std::abs(intersection.x) + std::abs(intersection.y);
				//For part 2
				int wirelength = it1.dist + std::abs(intersection.x - it1.start.x) + std::abs(intersection.y - it1.start.y)
					           + it2.dist + std::abs(intersection.x - it2.start.x) + std::abs(intersection.y - it2.start.y);
				m_intersections.push_back({ dist, wirelength });
			}
		}
	}
}

Day3::~Day3()
{
}

int Day3::Part1()
{
	int shortest = INT_MAX;
	for (auto it : m_intersections)
	{
			if (it.dist < shortest)
				shortest = it.dist;
	}
	return shortest;
}

int Day3::Part2()
{
	int shortest = INT_MAX;
	for (auto it : m_intersections)
	{
		if (it.wireLength < shortest)
			shortest = it.wireLength;
	}
	return shortest;
}

bool Day3::TestIntersection(Line line1, Line line2, Pos& intersection)
{
	intersection = { 0,0 };

	if (line1.direction == line2.direction)
		return false;
	if (std::max(line2.start.x, line2.end.x) < std::min(line1.start.x, line1.end.x))
		return false;
	if (std::min(line2.start.x, line2.end.x) > std::max(line1.start.x, line1.end.x))
		return false;
	if (std::max(line2.start.y, line2.end.y) < std::min(line1.start.y, line1.end.y))
		return false;
	if (std::min(line2.start.y, line2.end.y) > std::max(line1.start.y, line1.end.y))
		return false;
	
	if (line1.direction == Dir::Horizonal)
		intersection = { line2.start.x, line1.start.y };
	else
		intersection = { line1.start.x, line2.start.y };
	return true;
}

Day3::Pos Day3::NextPoint(Pos start, std::pair<char, int> move)
{
	switch (move.first)
	{
	case 'U':
		start.y += move.second;
		return start;
	case 'D':
		start.y -= move.second;
		return start;
	case 'L':
		start.x -= move.second;
		return start;
	case 'R':
		start.x += move.second;
		return start;
	}
	std::cout << "Error getting next point" << std::endl;
	return start;
}

void Day3::createWire(std::vector<Line>& wire, std::vector<std::pair<char, int>> & moveData)
{
	Pos pos = { 0,0 };
	int dist = 0;
	for (auto it : moveData)
	{
		auto newPos = NextPoint(pos, it);
		Dir direction;
		switch (it.first)
		{
		case 'U':
		case'D':
			direction = Dir::Vertical;
			break;
		case 'L':
		case'R':
			direction = Dir::Horizonal;
			break;
		}
		wire.push_back({ pos, newPos, direction, dist });
		dist += it.second;
		pos = newPos;
	}
}

void Day3::loadData(std::fstream & stream, std::vector < std::vector<std::pair<char, int>>>& data)
{
	std::string line;
	while (std::getline(stream, line))
	{
		data.emplace_back();
		auto& wire = data.back();
		std::istringstream iss(line);
		char dir;
		int dist;
		char sep;
		while (iss >> dir >> dist >> sep)
		{
			wire.push_back({ dir, dist });
		}
		iss >> dir >> dist;
		wire.push_back({ dir, dist });
	}
}
