#include "Day14.h"
#include <sstream>
#include <queue>

Day14::Day14()
{
	std::fstream stream(m_filename, std::fstream::in);
	loadData(stream, m_recipes);
	int bob = 1;
}

Day14::~Day14()
{
}

long long Day14::Part1()
{
	return makeFuel(1);
}

long long Day14::Part2()
{
	long long oreAvailable = 1000000000000L;
	long long min = oreAvailable / makeFuel(1);
	long long max = min * 2;
	return binSearch(min, max, oreAvailable);

}

long long Day14::makeFuel(long long amountOfFuel)
{
	std::map<std::string, long long> stock;
	std::queue<Ingredient> required;
	required.push({ amountOfFuel, "FUEL" });
	long long oreCount = 0;
	while (required.size())
	{
		auto& ingredient = required.front();
		auto& recipe = m_recipes[ingredient.name];
		long long& stockamount = stock[ingredient.name];
		if (stockamount)
		{
			if (stockamount < ingredient.amount)
			{
				ingredient.amount -= stockamount;
				stockamount = 0;
			}
			else
			{
				stockamount -= ingredient.amount;
				required.pop();
				continue;
			}
		}
		long long mult = ingredient.amount / recipe.amountMade;
		long long remainder = ingredient.amount % recipe.amountMade;
		if (remainder)
		{
			stockamount += recipe.amountMade - remainder;
			mult++;
		}
		for (auto newIngredient : recipe.ingredients)
		{
			if (newIngredient.name == "ORE")
			{
				oreCount += newIngredient.amount * mult;
			}
			else
			{
				newIngredient.amount *= mult;
				required.push(newIngredient);
			}
		}
		required.pop();
	}
	return oreCount;
}

long long Day14::binSearch(long long min, long long max, long long ore)
{
	long long middle = min + (max - min) / 2;

	while (min < max - 1)
	{
		long long oreUsed = makeFuel(middle);
		if (oreUsed < ore)
		{
			min = middle;
		}
		else
		{
			max = middle;
		}
		middle = min + (max - min) / 2;
	}

	return min;
}

void Day14::loadData(std::fstream & stream, std::map<std::string, Recipe> & recipes)
{
		long long amount = 0;
		std::string text;
		while (stream >> amount) //for each recipe
		{
			Recipe recipe;
			while (true) //While ingredients
			{
				stream >> text; //read ingredient name
				if (text[text.size() - 1] == ',')
				{
					text.erase(text.end() - 1);
				}
				recipe.ingredients.push_back({ amount ,text });
				stream >> text; //read next part
				if (text == "=>") //if output
					break;
				else
					amount = stoi(text); //we have another input amount
			}
			stream >> amount;
			recipe.amountMade = amount;
			stream >> text;
			m_recipes[text] = recipe;
		}
}
