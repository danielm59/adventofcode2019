#pragma once
#include <string>
#include <vector>
#include <map>
#include <fstream>
class Day14
{
public:
	Day14();
	~Day14();
	long long Part1();
	long long Part2();
private:
	struct Ingredient
	{
		long long amount;
		std::string name;
	};
	struct Recipe
	{
		long long amountMade;
		std::vector<Ingredient> ingredients;
	};
	std::map<std::string, Recipe> m_recipes;
	long long makeFuel(long long amountOfFuel);
	long long binSearch(long long min, long long max, long long ore);
	void loadData(std::fstream& stream, std::map<std::string, Recipe>& recipes);
	std::string m_filename = "Data/Day14.txt";
};

