#include "Day15.h"
#include <iostream>
#include <queue>

Day15::Day15()
{
}

Day15::~Day15()
{
}

int Day15::Part1()
{
	std::fstream datafile(m_filename, std::fstream::in);
	std::vector<long long> intcode;
	loadData(datafile, intcode);
	CPU cpu(intcode);
	std::queue<state> states;
	states.push({ cpu,1, Droid(128) }); // we start at 1 to remove the need to add 1 at case 2.
	bool con = true;
	long long output;
	int shortest = INT_MAX;
	while (states.size())
	{		
		for (int dir = 1; dir <= 4; dir++)
		{
			state cState = states.front();
			if (cState.length >= shortest) //we can stop testing a state when it's length is longer than the shortest found
				break;
			CPU& cpu = cState.cpu;
			cpu.run({ dir }, output);
			Droid& cDriod = cState.droid;
			switch (output)
			{
			case 0:
				break;
			case 1:
				if (cDriod.move(dir))
				{
					states.push({ cpu, cState.length + 1, cDriod });
				}
				break;
			case 2:
				if (cState.length < shortest)
				{
					shortest = cState.length;
					m_oxIntCode = cpu.intcode;
				}
			}
		}
		states.pop();
	}
	return shortest;
}

int Day15::Part2()
{
	CPU cpu(m_oxIntCode);
	std::queue<o2State> states;
	std::vector<std::vector<char>> map(128, std::vector<char>(128, ' '));
	states.push({ cpu,0, &map});
	bool con = true;
	long long output;
	int longest = 0;
	while (states.size())
	{
		for (int dir = 1; dir <= 4; dir++)
		{
			o2State cState = states.front();
			CPU& cpu = cState.cpu;
			cpu.run({ dir }, output);
			o2& cO2 = cState.o2;
			switch (output)
			{
			case 0:
				cO2.move(dir, '#');
				if (cState.length > longest)
					longest = cState.length;
				break;
			case 1:
				if (cO2.move(dir, 'o'))
				{
					states.push({ cpu, cState.length + 1, cO2 });
				}
				break;
			}
		}
		states.pop();
	}
	return longest;
}

void Day15::loadData(std::fstream & stream, std::vector<long long>& data)
{
	long long i = 0L;
	char c;
	while (stream >> i >> c)
	{
		data.push_back(i);
	}
	stream >> i;
	data.push_back(i);
}

bool Day15::CPU::run(std::vector<long long> input, long long & output)
{
	int inputID = 0;
	for (int& i = pos; i < intcode.size();)
	{
		long long operation = intcode[i];
		int opcode = operation % 100;
		switch (opcode)
		{
		case 1:
			loadParams(operation, 3);
			*p3 = *p1 + *p2;
			i += 4;
			break;
		case 2:
			loadParams(operation, 3);
			*p3 = *p1 * *p2;
			i += 4;
			break;
		case 3:
			loadParams(operation, 1);
			*p1 = input[inputID++];
			i += 2;
			break;
		case 4:
			loadParams(operation, 1);
			output = *p1;
			i += 2;
			return true;
			break;
		case 5:
			loadParams(operation, 2);
			i = *p1 ? *p2 : i + 3;
			break;
		case 6:
			loadParams(operation, 2);
			i = *p1 ? i + 3 : *p2;
			break;
		case 7:
			loadParams(operation, 3);
			*p3 = (*p1 < *p2) ? 1 : 0;
			i += 4;
			break;
		case 8:
			loadParams(operation, 3);
			*p3 = (*p1 == *p2) ? 1 : 0;
			i += 4;
			break;
		case 9:
			loadParams(operation, 1);
			rBase += *p1;
			i += 2;
			break;
		case 99:
			return false;
		default:
			std::cout << "Error, Unknow Opcode:" << i << "," << opcode << std::endl;
			return intcode[0];
			break;
		}
	}
	std::cout << "Error, Reached End:" << pos << std::endl;
	return intcode[0];
}

void Day15::CPU::loadParams(long long operation, int numberToLoad)
{
	switch (numberToLoad)
	{
	case 3:
		p3 = loadParam(operation / 10000, pos + 3);
	case 2:
		p2 = loadParam((operation / 1000) % 10, pos + 2);
	case 1:
		p1 = loadParam((operation / 100) % 10, pos + 1);
	}
}

long long * Day15::CPU::loadParam(int parammode, int pos)
{
	switch (parammode)
	{
	case 0:
		return &intcode[intcode[pos]];
	case 1:
		return &intcode[pos];
	case 2:
		return &intcode[rBase + intcode[pos]];
	}
}
