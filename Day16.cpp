#include "Day16.h"
#include <sstream>

Day16::Day16()
{
}

Day16::~Day16()
{
}

std::string Day16::Part1()
{
	std::fstream stream(m_filename, std::fstream::in);
	std::vector<int> data;
	loadData(stream, data);
	FFT(data);
	std::stringstream output;
	for (int i = 0; i < 8; i++)
		output << data[i];
	return output.str();
}

std::string Day16::Part2()
{
	std::fstream stream(m_filename, std::fstream::in);
	std::vector<int> data;
	loadData(stream, data);
	std::vector<int> superData;

	int offset = 0;

	for (int i = 0; i < 7; ++i)
		offset = offset * 10 + data[i];

	for (int i = 0; i < 10000; ++i)
		superData.insert(superData.end(), data.begin(), data.end());

	for (int run = 0; run < 100; ++run)
	{
		int val = superData.back();
		for (int index = superData.size() - 2; index >= offset; --index)
		{
			val += superData[index];
			val = val % 10;
			superData[index] = val;
		}
	}
	std::stringstream output;
	for (int i = offset; i < offset + 8; i++)
		output << superData[i];
	return output.str();

}

void Day16::FFT(std::vector<int>& data)
{
	int dataSize = data.size();
	int patternSize = pattern.size();
	for (int n = 0; n < 100; n++)
	{
		std::vector<int> newData(dataSize, 0);
		for (int i = 0; i < dataSize; i++)
		{
			int sum = 0;
			for (int j = 0; j < dataSize; j++)
			{
				int mult = pattern[((j + 1) / (i + 1)) % patternSize];
				sum += data[j] * mult;
			}
			newData[i] = abs(sum) % 10;
		}
		data = newData;
	}
}

void Day16::loadData(std::fstream & stream, std::vector<int>& data)
{
	char n;
	while (stream >> n)
	{
		data.push_back(n - '0');
	}
}
