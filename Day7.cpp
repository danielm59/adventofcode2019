#include "Day7.h"
#include <iostream>
#include <fstream>
#include <algorithm>

Day7::Day7()
{
}

Day7::~Day7()
{
}

int Day7::Part1()
{
	std::fstream datafile(m_filename, std::fstream::in);
	std::vector<int> intcode;
	loadData(datafile, intcode);

	int largest = 0;
	std::vector<int> order = { 0,1,2,3,4 };
	while (std::next_permutation(order.begin(), order.end()))
	{
		CPU cpu1(intcode);
		CPU cpu2(intcode);
		CPU cpu3(intcode);
		CPU cpu4(intcode);
		CPU cpu5(intcode);

		int val = 0;
		runIntCode(cpu1, { order[0],val }, val);
		runIntCode(cpu2, { order[1],val }, val);
		runIntCode(cpu3, { order[2],val }, val);
		runIntCode(cpu4, { order[3],val }, val);
		runIntCode(cpu5, { order[4],val }, val);
		if (val > largest)
			largest = val;
	}
	return largest;
}

int Day7::Part2()
{
	std::fstream datafile(m_filename, std::fstream::in);
	std::vector<int> intcode;
	loadData(datafile, intcode);

	int largest = 0;
	std::vector<int> order = { 5,6,7,8,9 };
	while (std::next_permutation(order.begin(), order.end()))
	{
		CPU cpu1(intcode);
		CPU cpu2(intcode);
		CPU cpu3(intcode);
		CPU cpu4(intcode);
		CPU cpu5(intcode);

		int val = 0;
		runIntCode(cpu1, { order[0],val }, val);
		runIntCode(cpu2, { order[1],val }, val);
		runIntCode(cpu3, { order[2],val }, val);
		runIntCode(cpu4, { order[3],val }, val);
		bool con = runIntCode(cpu5, { order[4],val }, val);

		while (con)
		{
			runIntCode(cpu1, { val }, val);
			runIntCode(cpu2, { val }, val);
			runIntCode(cpu3, { val }, val);
			runIntCode(cpu4, { val }, val);
			con = runIntCode(cpu5, { val }, val);
		}
		if (val > largest)
			largest = val;
	}

	return largest;
}


bool Day7::runIntCode(CPU& cpu, std::vector<int> input, int& output)
{
	int inputID = 0;
	std::vector<int>& intcode = cpu.intcode;
	for (int& i = cpu.pos; i < intcode.size();)
	{
		int operation = intcode[i];
		int opcode = operation % 100;
		int*p1 = nullptr;
		int*p2 = nullptr;
		int*p3 = nullptr;
		switch (opcode)
		{
		case 1:
			p1 = ((operation / 100) % 10) ? &intcode[i + 1] : &intcode[intcode[i + 1]];
			p2 = ((operation / 1000) % 10) ? &intcode[i + 2] : &intcode[intcode[i + 2]];
			p3 = (operation / 10000) ? &intcode[i + 3] : &intcode[intcode[i + 3]];
			*p3 = *p1 + *p2;
			i += 4;
			break;
		case 2:
			p1 = ((operation / 100) % 10) ? &intcode[i + 1] : &intcode[intcode[i + 1]];
			p2 = ((operation / 1000) % 10) ? &intcode[i + 2] : &intcode[intcode[i + 2]];
			p3 = (operation / 10000) ? &intcode[i + 3] : &intcode[intcode[i + 3]];
			*p3 = *p1 * *p2;
			i += 4;
			break;
		case 3:
			p1 = ((operation / 100) % 10) ? &intcode[i + 1] : &intcode[intcode[i + 1]];
			*p1 = input[inputID++];
			i += 2;
			break;
		case 4:
			p1 = ((operation / 100) % 10) ? &intcode[i + 1] : &intcode[intcode[i + 1]];
			output = *p1;
			i += 2;
			return true;
			break;
		case 5:
			p1 = ((operation / 100) % 10) ? &intcode[i + 1] : &intcode[intcode[i + 1]];
			p2 = ((operation / 1000) % 10) ? &intcode[i + 2] : &intcode[intcode[i + 2]];
			i = *p1 ? *p2 : i + 3;
			break;
		case 6:
			p1 = ((operation / 100) % 10) ? &intcode[i + 1] : &intcode[intcode[i + 1]];
			p2 = ((operation / 1000) % 10) ? &intcode[i + 2] : &intcode[intcode[i + 2]];
			i = *p1 ? i + 3 : *p2;
			break;
		case 7:
			p1 = ((operation / 100) % 10) ? &intcode[i + 1] : &intcode[intcode[i + 1]];
			p2 = ((operation / 1000) % 10) ? &intcode[i + 2] : &intcode[intcode[i + 2]];
			p3 = (operation / 10000) ? &intcode[i + 3] : &intcode[intcode[i + 3]];
			*p3 = (*p1 < *p2) ? 1 : 0;
			i += 4;
			break;
		case 8:
			p1 = ((operation / 100) % 10) ? &intcode[i + 1] : &intcode[intcode[i + 1]];
			p2 = ((operation / 1000) % 10) ? &intcode[i + 2] : &intcode[intcode[i + 2]];
			p3 = (operation / 10000) ? &intcode[i + 3] : &intcode[intcode[i + 3]];
			*p3 = (*p1 == *p2) ? 1 : 0;
			i += 4;
			break;
		case 99:
			return false;
		default:
			std::cout << "Error, Unknow Opcode:" << i << "," << opcode << std::endl;
			return intcode[0];
			break;
		}
	}
	std::cout << "Error, Reached End" << std::endl;
	return intcode[0];
}

void Day7::loadData(std::fstream & stream, std::vector<int> & data)
{
	int i = 0;
	char c;
	while (stream >> i >> c)
	{
		data.push_back(i);
	}
	stream >> i;
	data.push_back(i);
}
