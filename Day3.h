#pragma once
#include <string>
#include <vector>
#include <fstream>
class Day3
{
public:
	Day3();
	~Day3();
	int Part1();
	int Part2();
private:
	struct Pos
	{
		int x;
		int y;
	};
	struct Intersection
	{
		int dist;
		int wireLength;
	};
	enum Dir
	{
		Horizonal, Vertical
	};
	struct Line
	{
		Pos start;
		Pos end;
		Dir direction;
		int dist;
	};
	bool TestIntersection(Line line1, Line line2, Pos& intersection);
	Pos NextPoint(Pos start, std::pair<char, int> move);
	void createWire(std::vector<Line>& wire, std::vector < std::pair<char, int>> & moveData);
	void loadData(std::fstream& stream, std::vector <std::vector<std::pair<char, int>>> & data);
	std::string m_filename = "Data/Day3.txt";
	std::vector< Intersection> m_intersections;
};

