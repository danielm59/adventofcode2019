#pragma once
#include <string>
#include <vector>
#include <fstream>
class Day5
{
public:
	Day5();
	~Day5();
	void Part1();
	void Part2();
private:
	int runIntCode(std::vector<int> intcode, int input);
	void loadData(std::fstream & stream, std::vector<int> & data);
	std::string m_filename = "Data/Day5.txt";
};

