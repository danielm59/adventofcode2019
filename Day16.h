#pragma once
#include <fstream>
#include <string>
#include <vector>
class Day16
{
public:
	Day16();
	~Day16();
	std::string Part1();
	std::string Part2();
private:
	std::vector<int> pattern = { 0,1,0,-1 };
	void FFT(std::vector<int>& data);
	void loadData(std::fstream & stream, std::vector<int> & data);
	std::string m_filename = "Data/Day16.txt";
};

