#pragma once
#define _USE_MATH_DEFINES
#include <math.h>
#include <vector>
#include <string>
#include <fstream>
class Day10
{
public:
	Day10();
	~Day10();
	int Part1();
	int Part2();
private:
	struct asteroid
	{
		int x;
		int y;
		double dist;
		double angle;
		bool boom = false;
	};
	double getAngle(asteroid pos1, asteroid pos2);
	double getDist(asteroid pos1, asteroid pos2);
	void loadData(std::fstream & stream, std::vector<asteroid> & data);
	std::string m_filename = "Data/Day10.txt";
	asteroid m_scannerLocation;
	std::vector<asteroid> m_asteroids;
	double _2PI = M_PI * 2.0;
};

