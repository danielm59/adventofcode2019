#pragma once
#include <string>
#include <fstream>
#include <vector>

class Day2
{
public:
	Day2();
	~Day2();
	int Part1();
	int Part2();
private:
	void loadData(std::fstream& stream, std::vector<int> & data);
	int runIntCode(std::vector<int> intcode, int pos1, int pos2);
	std::string m_filename = "Data/Day2.txt";
};

