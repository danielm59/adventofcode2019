#include "Day13.h"
#include <fstream>
#include <iostream>

Day13::Day13()
{
}

Day13::~Day13()
{
}

int Day13::Part1()
{
	std::fstream datafile(m_filename, std::fstream::in);
	std::vector<long long> intcode;
	loadData(datafile, intcode);
	CPU cpu(intcode);
	bool con = true;
	int count = 0;
	long long output;
	while (con)
	{
		con = cpu.run({}, output);
		con = cpu.run({}, output);
		con = cpu.run({}, output);
		if (output == 2L)
			count++;
	}
	return count;
}

int Day13::Part2()
{
	std::fstream datafile(m_filename, std::fstream::in);
	std::vector<long long> intcode;
	loadData(datafile, intcode);
	intcode[0] = 2; //insert a quarter
	CPU cpu(intcode);
	bool con = true;
	int count = 0;
	long long x, y, val;
	pos ball = { 0L,0L };
	pos paddle = { 0L,0L };
	long long score;
	while (con)
	{
		con = cpu.run({ compare(paddle.x, ball.x) }, x);
		con = cpu.run({ compare(paddle.x, ball.x) }, y);
		con = cpu.run({ compare(paddle.x, ball.x) }, val);
		if (x == -1 and y == 0)
			score = val;
		else if (val == 3)
			paddle = { x,y };
		else if (val == 4)
			ball = { x,y };
	}
	return score;
}

int Day13::compare(long long a, long long b)
{
	if (a < b)
		return 1;
	if (a > b)
		return -1;
	return 0;
}

void Day13::loadData(std::fstream & stream, std::vector<long long>& data)
{
	long long i = 0L;
	char c;
	while (stream >> i >> c)
	{
		data.push_back(i);
	}
	stream >> i;
	data.push_back(i);
}

bool Day13::CPU::run(std::vector<long long> input, long long & output)
{
	int inputID = 0;
	for (int& i = pos; i < intcode.size();)
	{
		long long operation = intcode[i];
		int opcode = operation % 100;
		switch (opcode)
		{
		case 1:
			loadParams(operation, 3);
			*p3 = *p1 + *p2;
			i += 4;
			break;
		case 2:
			loadParams(operation, 3);
			*p3 = *p1 * *p2;
			i += 4;
			break;
		case 3:
			loadParams(operation, 1);
			*p1 = input[inputID++];
			i += 2;
			break;
		case 4:
			loadParams(operation, 1);
			output = *p1;
			i += 2;
			return true;
			break;
		case 5:
			loadParams(operation, 2);
			i = *p1 ? *p2 : i + 3;
			break;
		case 6:
			loadParams(operation, 2);
			i = *p1 ? i + 3 : *p2;
			break;
		case 7:
			loadParams(operation, 3);
			*p3 = (*p1 < *p2) ? 1 : 0;
			i += 4;
			break;
		case 8:
			loadParams(operation, 3);
			*p3 = (*p1 == *p2) ? 1 : 0;
			i += 4;
			break;
		case 9:
			loadParams(operation, 1);
			rBase += *p1;
			i += 2;
			break;
		case 99:
			return false;
		default:
			std::cout << "Error, Unknow Opcode:" << i << "," << opcode << std::endl;
			return intcode[0];
			break;
		}
	}
	std::cout << "Error, Reached End:" << pos << std::endl;
	return intcode[0];
}

void Day13::CPU::loadParams(long long operation, int numberToLoad)
{
	switch (numberToLoad)
	{
	case 3:
		p3 = loadParam(operation / 10000, pos + 3);
	case 2:
		p2 = loadParam((operation / 1000) % 10, pos + 2);
	case 1:
		p1 = loadParam((operation / 100) % 10, pos + 1);
	}
}

long long * Day13::CPU::loadParam(int parammode, int pos)
{
	switch (parammode)
	{
	case 0:
		return &intcode[intcode[pos]];
	case 1:
		return &intcode[pos];
	case 2:
		return &intcode[rBase + intcode[pos]];
	}
}