#pragma once
#include <string>
#include <fstream>
#include <vector>
class Day12
{
public:
	Day12();
	~Day12();
	int Part1();
	long long Part2();
private:
	struct v3
	{
		int x = 0;
		int y = 0;
		int z = 0;
		v3 operator += (v3 const &obj) {
			x += obj.x;
			y += obj.y;
			z += obj.z;
			return *this;
		}
	};
	struct v3L
	{
		long long x = 0;
		long long y = 0;
		long long z = 0;
	};
	struct Moon
	{
		v3 pos;
		v3 vel = { 0,0,0 };
		void calcGravity(Moon otherMoon);
		int compare(int a, int b);
		int energy();
	};
	void step(std::vector<Moon>& moons);
	void loadData(std::fstream & stream, std::vector<Moon> & moons);
	std::string m_filename = "Data/Day12.txt";
};

