#include "Day8.h"
#include <fstream>
#include <algorithm>
#include <iostream>

Day8::Day8()
{
}


Day8::~Day8()
{
}

int Day8::Part1()
{
	std::vector<std::string> layers = loadLayers();
	int least = INT_MAX;
	std::string leastLayer;
	for (auto layer : layers)
	{
		auto count = std::count(layer.begin(), layer.end(), '0');
		if (count < least)
		{
			least = count;
			leastLayer = layer;
		}
	}

	auto ones = std::count(leastLayer.begin(), leastLayer.end(), '1');
	auto twos = std::count(leastLayer.begin(), leastLayer.end(), '2');
	return ones * twos;
}

void Day8::Part2()
{
	std::vector<std::string> layers = loadLayers();
	std::string image = layers[layers.size() - 1];
	for (int i = layers.size() - 2; i >= 0; i--)
	{
		updateImage(image, layers[i]);
	}
	std::transform(image.begin(), image.end(), image.begin(),
		[](char c) -> char { 
		switch (c)
		{
		case '0':
			return ' ';
		case '1':
			return '*';
		}
	});
	std::vector<std::string> finalImage = toImage(image);
	for (auto row : finalImage)
	{
		std::cout << row << std::endl;
	}
}

std::vector<std::string> Day8::loadLayers()
{
	std::fstream stream(m_filename, std::fstream::in);
	std::vector<std::string> layers;
	std::string input;
	while (stream >> input)
	{
		int layerSize = 25 * 6;
		int layerCount = input.length() / layerSize;
		for (auto i = 0, offset = 0; i < layerCount; ++i)
		{
			layers.push_back(input.substr(offset, layerSize));
			offset += layerSize;
		}

	}
	return layers;
}

std::vector<std::string> Day8::toImage(std::string layer)
{
	std::vector<std::string> image;
	int rowSize = 25;
	int rows = 6;
	for (auto i = 0, offset = 0; i < rows; ++i)
	{
		image.push_back(layer.substr(offset, rowSize));
		offset += rowSize;
	}
	return image;
}

void Day8::updateImage(std::string& image, std::string layer)
{
	for (auto i = 0; i < layer.size(); i++)
	{
		if (layer[i] != '2')
			image[i] = layer[i];
	}
}
