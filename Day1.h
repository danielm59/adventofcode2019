#pragma once
#include <string>
class Day1
{
public:
	Day1();
	~Day1();
	int part1();
	int part2();
private:
	int calculateFuel(int weight);
	std::string m_filename = "Data/Day1.txt";
};

