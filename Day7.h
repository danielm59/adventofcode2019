#pragma once
#include <string>
#include <vector>
class Day7
{
public:
	Day7();
	~Day7();
	int Part1();
	int Part2();
private:
	struct CPU
	{
		std::vector<int> intcode;
		int pos = 0;
		CPU(std::vector<int> code) : intcode(code) {};
	};
	bool runIntCode(CPU& cpu, std::vector<int> input, int& output);
	void loadData(std::fstream & stream, std::vector<int> & data);
	std::string m_filename = "Data/Day7.txt";
};

