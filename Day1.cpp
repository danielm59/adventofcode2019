#include "Day1.h"
#include <iostream>
#include <fstream>

Day1::Day1()
{
}


Day1::~Day1()
{
}

/*
What is the sum of the fuel requirements for all of the modules on your spacecraft?
*/
int Day1::part1()
{
	int weight = 0;
	int totalFuel = 0;
	std::ifstream datafile(m_filename);
	if (datafile.is_open())
	{
		while ((datafile >> weight))
		{
			int fuel = calculateFuel(weight);
			totalFuel += fuel;
		}
		datafile.close();
	}
	else 
		std::cout << "Unable to open file" << std::endl;

	return totalFuel;
}

/*
What is the sum of the fuel requirements for all of the modules on your spacecraft when also taking into account the mass of the added fuel?
(Calculate the fuel requirements for each module separately, then add them all up at the end.)
*/
int Day1::part2()
{
	int weight = 0;
	int totalFuel = 0;
	std::ifstream datafile(m_filename);
	if (datafile.is_open())
	{
		while ((datafile >> weight))
		{
			int fuel = calculateFuel(weight);
			totalFuel += fuel;
			while ((fuel = calculateFuel(fuel)) > 0)
			{
				totalFuel += fuel;
			}
		}
		datafile.close();
	}
	else
		std::cout << "Unable to open file" << std::endl;

	return totalFuel;
}

/*
Fuel required to launch a given module is based on its mass. Specifically, to find the fuel required for a module, take its mass, divide by three, round down, and subtract 2.
*/
int Day1::calculateFuel(int weight)
{
	return (weight / 3) - 2;
}
