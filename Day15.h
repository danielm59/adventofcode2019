#pragma once
#include <vector>
#include <fstream>
class Day15
{
public:
	Day15();
	~Day15();
	int Part1();
	int Part2();
private:
	struct CPU
	{
		long long*p1 = nullptr;
		long long*p2 = nullptr;
		long long*p3 = nullptr;
		int rBase = 0;
		std::vector<long long> intcode;
		int pos = 0;
		CPU(std::vector<long long> code) : intcode(code)
		{
			intcode.resize(2048, 0L);
		};
		bool run(std::vector<long long> input, long long& output);
		void loadParams(long long operation, int numberToLoad);
		long long* loadParam(int parammode, int pos);
	};
	struct Droid
	{
		std::vector<int> moveX = { 0,-1,1,0,0 };
		std::vector<int> moveY = { 0,0,0,-1,1 };
		std::vector<std::vector<char>> map;
		int x;
		int y;
		Droid(int mapSize) : map(mapSize, std::vector<char>(mapSize, ' '))
		{
			x = mapSize / 2;
			y = mapSize / 2;
			map[x][y] = '.';
		};
		bool move(int dir)
		{
			x += moveX[dir];
			y += moveY[dir];
			char& pos = map[x][y];
			if (pos == ' ')
			{
				pos = '.';
				return true;
			}
			return false;
		}
	};
	struct o2
	{
		std::vector<int> moveX = { 0,-1,1,0,0 };
		std::vector<int> moveY = { 0,0,0,-1,1 };
		std::vector<std::vector<char>>* map;
		int x;
		int y;
		o2(std::vector<std::vector<char>>* _map) : map(_map)
		{
			int mapSize = map->size();
			x = mapSize / 2;
			y = mapSize / 2;
			(*map)[x][y] = '.';
		};
		bool move(int dir, char mapIcon)
		{
			x += moveX[dir];
			y += moveY[dir];
			char& pos = (*map)[x][y];
			if (pos == ' ')
			{
				pos = mapIcon;
				return true;
			}
			return false;
		}
	};
	struct state
	{
		CPU cpu;
		int length = 0;
		Droid droid;
	};

	struct o2State
	{
		CPU cpu;
		int length = 0;
		o2 o2;
	};
	std::vector<long long> m_oxIntCode;
	void loadData(std::fstream & stream, std::vector<long long> & data);
	std::string m_filename = "Data/Day15.txt";
};

