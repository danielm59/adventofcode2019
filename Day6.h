#pragma once
#include <string>
#include <map>
class Day6
{
public:
	Day6();
	~Day6();
	int Part1();
	int Part2();
private:
	struct node
	{
		node* parent = nullptr;
		int distFromYOU = -1;
	};
	std::map<std::string, node> dataMap;
	std::string m_filename = "Data/Day6.txt";
};

