#include "Day2.h"
#include <iostream>

Day2::Day2()
{
}

Day2::~Day2()
{
}

int Day2::Part1()
{
	std::fstream datafile(m_filename, std::fstream::in);
	std::vector<int> intcode;
	loadData(datafile, intcode);
	return runIntCode(intcode, 12, 2);
}

int Day2::Part2()
{
	std::fstream datafile(m_filename, std::fstream::in);
	std::vector<int> intcode;
	loadData(datafile, intcode);
	for (int i = 1; i < 100; i++)
	{
		for (int j = 1; j < 100; j++)
		{
			if (runIntCode(intcode, i, j) == 19690720)
			{
				return 100 * i + j;
			}
		}
	}
	std::cout << "Cound not find answer" << std::endl;
	return 0;
}

void Day2::loadData(std::fstream & stream, std::vector<int> & data)
{
	int i = 0;
	char c;
	while (stream >> i >> c)
	{
		data.push_back(i);
	}
	stream >> i;
	data.push_back(i);
}

int Day2::runIntCode(std::vector<int> intcode, int pos1, int pos2)
{
	intcode[1] = pos1;
	intcode[2] = pos2;
	for (int i = 0; i < intcode.size(); i += 4)
	{
		int opcode = intcode[i];
		switch (opcode)
		{
		case 1:
			intcode[intcode[i + 3]] = intcode[intcode[i + 1]] + intcode[intcode[i + 2]];
			break;
		case 2:
			intcode[intcode[i + 3]] = intcode[intcode[i + 1]] * intcode[intcode[i + 2]];
			break;
		case 99:
			return intcode[0];
		default:
			std::cout << "Error, Unknow Opcode" << std::endl;
			return intcode[0];
			break;
		}
	}
	std::cout << "Error, Reached End" << std::endl;
	return intcode[0];
}


