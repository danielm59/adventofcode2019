#include "Day11.h"
#include <fstream>
#include <iostream>
#include <map>

Day11::Day11()
{
}

Day11::~Day11()
{
}

int Day11::Part1()
{
	std::fstream datafile(m_filename, std::fstream::in);
	std::vector<long long> intcode;
	loadData(datafile, intcode);
	CPU cpu(intcode);
	std::map<std::pair<int, int>, bool> colours;
	Robot robot;
	bool con = true;
	long long output;
	while (con)
	{
		bool& colour = colours[{robot.x, robot.y}];
		con = cpu.run({ colour }, output);
		if (!con)
			break;
		colour = output;
		con = cpu.run({}, output);
		if (!con)
			break;
		output ? robot.clockwise() : robot.anticlockwise();
		robot.move();
	}
	return colours.size();
}

void Day11::Part2()
{
	std::fstream datafile(m_filename, std::fstream::in);
	std::vector<long long> intcode;
	loadData(datafile, intcode);
	CPU cpu(intcode);
	std::map<std::pair<int, int>, bool> colours;
	colours[{0, 0}] = true;
	Robot robot;
	bool con = true;
	long long output;
	int minX = 0;
	int maxX = 0;
	int minY = 0;
	int maxY = 0;
	while (con)
	{
		bool& colour = colours[{robot.x, robot.y}];
		con = cpu.run({ colour }, output);
		if (!con)
			break;
		colour = output;
		con = cpu.run({}, output);
		if (!con)
			break;
		output ? robot.clockwise() : robot.anticlockwise();
		robot.move();
		if (robot.x < minX)
			minX = robot.x;
		if (robot.x > maxX)
			maxX = robot.x;
		if (robot.y < minY)
			minY = robot.y;
		if (robot.y > maxY)
			maxY = robot.y;
	}
	std::vector<std::string> image;
	image.resize(maxY - minY + 1);
	for (auto& row : image)
	{
		row.resize(maxX - minX + 1, ' ');
	}
	for (auto point : colours)
	{
		if (point.second)
			image[point.first.second - minY][point.first.first - minX] = '*';
	}
	for (auto row : image)
	{
		std::cout << row << std::endl;
	}
}

void Day11::loadData(std::fstream & stream, std::vector<long long>& data)
{
	long long i = 0L;
	char c;
	while (stream >> i >> c)
	{
		data.push_back(i);
	}
	stream >> i;
	data.push_back(i);
}

bool Day11::CPU::run(std::vector<long long> input, long long & output)
{
	int inputID = 0;
	for (int& i = pos; i < intcode.size();)
	{
		long long operation = intcode[i];
		int opcode = operation % 100;
		switch (opcode)
		{
		case 1:
			loadParams(operation, 3);
			*p3 = *p1 + *p2;
			i += 4;
			break;
		case 2:
			loadParams(operation, 3);
			*p3 = *p1 * *p2;
			i += 4;
			break;
		case 3:
			loadParams(operation, 1);
			*p1 = input[inputID++];
			i += 2;
			break;
		case 4:
			loadParams(operation, 1);
			output = *p1;
			i += 2;
			return true;
			break;
		case 5:
			loadParams(operation, 2);
			i = *p1 ? *p2 : i + 3;
			break;
		case 6:
			loadParams(operation, 2);
			i = *p1 ? i + 3 : *p2;
			break;
		case 7:
			loadParams(operation, 3);
			*p3 = (*p1 < *p2) ? 1 : 0;
			i += 4;
			break;
		case 8:
			loadParams(operation, 3);
			*p3 = (*p1 == *p2) ? 1 : 0;
			i += 4;
			break;
		case 9:
			loadParams(operation, 1);
			rBase += *p1;
			i += 2;
			break;
		case 99:
			return false;
		default:
			std::cout << "Error, Unknow Opcode:" << i << "," << opcode << std::endl;
			return intcode[0];
			break;
		}
	}
	std::cout << "Error, Reached End:" << pos << std::endl;
	return intcode[0];
}

void Day11::CPU::loadParams(long long operation, int numberToLoad)
{
	switch (numberToLoad)
	{
	case 3:
		p3 = loadParam(operation / 10000, pos + 3);
	case 2:
		p2 = loadParam((operation / 1000) % 10, pos + 2);
	case 1:
		p1 = loadParam((operation / 100) % 10, pos + 1);
	}
}

long long * Day11::CPU::loadParam(int parammode, int pos)
{
	switch (parammode)
	{
	case 0:
		return &intcode[intcode[pos]];
	case 1:
		return &intcode[pos];
	case 2:
		return &intcode[rBase + intcode[pos]];
	}
}