#include "Day4.h"
#include <string>


Day4::Day4()
{
}


Day4::~Day4()
{
}

int Day4::Part1()
{
	int count = 0;
	for (int n = 197487; n <= 673251; n++)
	{
		std::string cNumber = std::to_string(n);
		bool neverDecrease = true;
		bool adjacent = false;
		for (int i = 0; i < cNumber.size() - 1; i++)
		{
			if (cNumber[i] > cNumber[i + 1])
			{
				neverDecrease = false;
				break;
			}
			if (cNumber[i] == cNumber[i + 1])
				adjacent = true;
		 }
		if (neverDecrease && adjacent)
			count++;
	}
	return count;
}

int Day4::Part2()
{
	int count = 0;
	for (int n = 197487; n <= 673251; n++)
	{
		std::string cNumber = std::to_string(n);
		bool neverDecrease = true;
		bool adjacent = false;
		int max = cNumber.size() - 1;
		for (int i = 0; i < max; i++)
		{
			if (cNumber[i] > cNumber[i + 1])
			{
				neverDecrease = false;
				break;
			}
			if (cNumber[i] == cNumber[i + 1] && (i == 0 || cNumber[i] != cNumber[i-1]) && (i == max || cNumber[i] != cNumber[i + 2]))
				adjacent = true;
		}
		if (neverDecrease && adjacent)
			count++;
	}
	return count;
}
