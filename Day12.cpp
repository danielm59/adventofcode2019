#include "Day12.h"
#include <map>
#include <iostream>
#include <numeric>

Day12::Day12()
{
}


Day12::~Day12()
{
}

int Day12::Part1()
{
	std::vector<Moon> moons;
	std::fstream data(m_filename);
	loadData(data, moons);
	for (int n = 0; n < 1000; n++)
	{
		for (int i = 0; i < moons.size(); i++)
		{
			Moon& cMoon1 = moons[i];
			for (int j = i + 1; j < moons.size(); j++)
			{
				Moon& cMoon2 = moons[j];
				cMoon1.calcGravity(cMoon2);
				cMoon2.calcGravity(cMoon1);
			}
		}
		for (auto& moon : moons)
		{
			moon.pos += moon.vel;
		}
	}
	int energy = 0;
	for (auto moon : moons)
	{
		energy += moon.energy();
	}
	return energy;
}

long long Day12::Part2()
{
	//X
	std::vector<Moon> moons;
	std::fstream data(m_filename);
	loadData(data, moons);
	std::map<std::vector<int>, int> visited;
	int n = 0;
	v3L cycles;
	while (true)
	{
		std::vector<int> currentX;
		for (auto& moon : moons)
		{
			currentX.push_back(moon.pos.x);
			currentX.push_back(moon.vel.x);
		}
		if (visited.count(currentX))
		{
			std::cout << "X repeated " << visited[currentX] << " & " << n <<std::endl;
			cycles.x = n;
			break;
		}
		visited[currentX] = n;
		step(moons);
		n++;
	}

	//Y
	loadData(data, moons);
	visited.clear();
	n = 0;
	while (true)
	{
		std::vector<int> currentY;
		for (auto& moon : moons)
		{
			currentY.push_back(moon.pos.y);
			currentY.push_back(moon.vel.y);
		}
		if (visited.count(currentY))
		{
			std::cout << "Y repeated " << visited[currentY] << " & " << n << std::endl;
			cycles.y = n;
			break;
		}
		visited[currentY] = n;
		step(moons);
		n++;
	}

	//Z
	loadData(data, moons);
	visited.clear();
	n = 0;
	while (true)
	{
		std::vector<int> currentZ;
		for (auto& moon : moons)
		{
			currentZ.push_back(moon.pos.z);
			currentZ.push_back(moon.vel.z);
		}
		if (visited.count(currentZ))
		{
			std::cout << "Z repeated " << visited[currentZ] << " & " << n << std::endl;
			cycles.z = n;
			break;
		}
		visited[currentZ] = n;
		step(moons);
		n++;
	}
	return std::lcm(std::lcm(cycles.x, cycles.y), cycles.z);
}

void Day12::step(std::vector<Moon>& moons)
{
	for (int i = 0; i < moons.size(); i++)
	{
		Moon& cMoon1 = moons[i];
		for (int j = i + 1; j < moons.size(); j++)
		{
			Moon& cMoon2 = moons[j];
			cMoon1.calcGravity(cMoon2);
			cMoon2.calcGravity(cMoon1);
		}
	}
	for (auto& moon : moons)
	{
		moon.pos += moon.vel;
	}
}

void Day12::loadData(std::fstream & stream, std::vector<Moon> & moons)
{
	for (std::string line; std::getline(stream, line); )
	{
		Moon moon;
		sscanf_s(line.c_str(), "<x=%d, y=%d, z=%d>", &moon.pos.x, &moon.pos.y, &moon.pos.z);
		moons.push_back(moon);
	}
}

void Day12::Moon::calcGravity(Moon otherMoon)
{
	vel.x += compare(pos.x, otherMoon.pos.x);
	vel.y += compare(pos.y, otherMoon.pos.y);
	vel.z += compare(pos.z, otherMoon.pos.z);
}

int Day12::Moon::compare(int a, int b)
{
	if (a < b)
		return 1;
	if (a > b)
		return -1;
	return 0;
}

int Day12::Moon::energy()
{
	return (abs(pos.x)+ abs(pos.y)+ abs(pos.z))*(abs(vel.x) + abs(vel.y) + abs(vel.z));
}
