#include "Day5.h"
#include <iostream>

Day5::Day5()
{
}

Day5::~Day5()
{
}

void Day5::Part1()
{
	std::fstream datafile(m_filename, std::fstream::in);
	std::vector<int> intcode;
	loadData(datafile, intcode);
	runIntCode(intcode, 1);
}

void Day5::Part2()
{
	std::fstream datafile(m_filename, std::fstream::in);
	std::vector<int> intcode;
	loadData(datafile, intcode);
	runIntCode(intcode, 5);
}

int Day5::runIntCode(std::vector<int> intcode, int input)
{
	for (int i = 0; i < intcode.size();)
	{
		int operation = intcode[i];
		int opcode = operation % 100;
		int*p1 = nullptr;
		int*p2 = nullptr;
		int*p3 = nullptr;
		switch (opcode)
		{
		case 1:
			p1 = ((operation / 100) % 10) ? &intcode[i + 1] : &intcode[intcode[i + 1]];
			p2 = ((operation / 1000) % 10) ? &intcode[i + 2] : &intcode[intcode[i + 2]];
			p3 = (operation / 10000) ? &intcode[i + 3] : &intcode[intcode[i + 3]];
			*p3 = *p1 + *p2;
			i += 4;
			break;
		case 2:
			p1 = ((operation / 100) % 10) ? &intcode[i + 1] : &intcode[intcode[i + 1]];
			p2 = ((operation / 1000) % 10) ? &intcode[i + 2] : &intcode[intcode[i + 2]];
			p3 = (operation / 10000) ? &intcode[i + 3] : &intcode[intcode[i + 3]];
			*p3 = *p1 * *p2;
			i += 4;
			break;
		case 3:
			p1 = ((operation / 100) % 10) ? &intcode[i + 1] : &intcode[intcode[i + 1]];
			*p1 = input;
			i += 2;
			break;
		case 4:
			p1 = ((operation / 100) % 10) ? &intcode[i + 1] : &intcode[intcode[i + 1]];
			std::cout << *p1 << std::endl;
			i += 2;
			break;
		case 5:
			p1 = ((operation / 100) % 10) ? &intcode[i + 1] : &intcode[intcode[i + 1]];
			p2 = ((operation / 1000) % 10) ? &intcode[i + 2] : &intcode[intcode[i + 2]];
			i = *p1 ? *p2 : i + 3;
			break;
		case 6:
			p1 = ((operation / 100) % 10) ? &intcode[i + 1] : &intcode[intcode[i + 1]];
			p2 = ((operation / 1000) % 10) ? &intcode[i + 2] : &intcode[intcode[i + 2]];
			i = *p1 ? i + 3 : *p2;
			break;
		case 7:
			p1 = ((operation / 100) % 10) ? &intcode[i + 1] : &intcode[intcode[i + 1]];
			p2 = ((operation / 1000) % 10) ? &intcode[i + 2] : &intcode[intcode[i + 2]];
			p3 = (operation / 10000) ? &intcode[i + 3] : &intcode[intcode[i + 3]];
			*p3 = (*p1 < *p2) ? 1 : 0;
			i += 4;
			break;
		case 8:
			p1 = ((operation / 100) % 10) ? &intcode[i + 1] : &intcode[intcode[i + 1]];
			p2 = ((operation / 1000) % 10) ? &intcode[i + 2] : &intcode[intcode[i + 2]];
			p3 = (operation / 10000) ? &intcode[i + 3] : &intcode[intcode[i + 3]];
			*p3 = (*p1 == *p2) ? 1 : 0;
			i += 4;
			break;
		case 99:
			return intcode[0];
		default:
			std::cout << "Error, Unknow Opcode:" << i << "," << opcode << std::endl;
			return intcode[0];
			break;
		}
	}
	std::cout << "Error, Reached End" << std::endl;
	return intcode[0];
}

void Day5::loadData(std::fstream & stream, std::vector<int> & data)
{
	int i = 0;
	char c;
	while (stream >> i >> c)
	{
		data.push_back(i);
	}
	stream >> i;
	data.push_back(i);
}
