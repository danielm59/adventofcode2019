#include "Day6.h"
#include <fstream>

Day6::Day6()
{
	std::ifstream data(m_filename);
	for (std::string line; std::getline(data, line); ) {
		char parent[4];
		char child[4];
		sscanf_s(line.c_str(), "%3s)%3s", parent, (unsigned)_countof(parent), child, (unsigned)_countof(child));
		dataMap[child].parent = &dataMap[parent];
	}
}

Day6::~Day6()
{
}

int Day6::Part1()
{
	int count = 0;
	for (auto& it : dataMap)
	{
		auto* nextNode = it.second.parent;
		while (nextNode != nullptr)
		{
			count++;
			nextNode = nextNode->parent;
		}
	}
	return count;
}

int Day6::Part2()
{
	auto you = dataMap["YOU"];
	int dist = 0;
	auto* cNode = you.parent;
	while (cNode != nullptr)
	{
		cNode->distFromYOU = dist++;
		cNode = cNode->parent;
	}
	auto santa = dataMap["SAN"];
	dist = 0;
	cNode = santa.parent;
	while (cNode != nullptr)
	{
		if (cNode->distFromYOU > 0)
		{
			return cNode->distFromYOU + dist;
		}
		dist++;
		cNode = cNode->parent;
	}
	return 0;
}
