#pragma once
#include <string>
#include <vector>
class Day8
{
public:
	Day8();
	~Day8();
	int Part1();
	void Part2();
private:
	std::vector<std::string> loadLayers();
	std::vector<std::string> toImage(std::string layer);
	void updateImage(std::string& image, std::string layer);
	std::string m_filename = "Data/Day8.txt";
};

